﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrickLaser : MonoBehaviour
{

    public LineRenderer lr;
    public GameObject prefab;


    void Start()
    {
        lr.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            lr.enabled = true;
            fireLaser();
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            lr.enabled = false;
        }
    }

    private void fireLaser()
    {
        float distance = 10.0f;

        RaycastHit hit;
        if (Physics.Raycast(lr.gameObject.transform.position, lr.gameObject.transform.forward, out hit, distance))
        {
            if (hit.collider.gameObject.tag == "box")
            {
                distance = hit.distance;
                Instantiate(prefab, hit.point, Quaternion.identity);
            }
        }


        lr.SetPosition(0, lr.gameObject.transform.position);
        lr.SetPosition(1, lr.gameObject.transform.position + new Vector3(0, 0, distance));
        lr.material.color = Color.red;
        lr.startWidth = 0.1f;

    }
}
