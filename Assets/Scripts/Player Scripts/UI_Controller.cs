﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour {    
	
	/*
	void Update ()
    {
        curHealth = GetComponentInParent<Drone_Stats>().curHealth;
        GetComponent<Slider>().value = curHealth;
	}
	*/

	//this is now being called only when the curHealth value changes
	public void setHealthSlider(int value)
	{
		GetComponent<Slider>().value = value;
	}
}
