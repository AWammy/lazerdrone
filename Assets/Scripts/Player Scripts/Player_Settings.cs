﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Settings: MonoBehaviour {

    //Aim Settings
    public float xSensitivity;
    public float ySensitivity;
    public float controllerAimMultiplier;
    public float scopedAimMultiplier;

    //Not Implimented Yet
    public float generalSensitivity;
    public bool invertAim;
}
