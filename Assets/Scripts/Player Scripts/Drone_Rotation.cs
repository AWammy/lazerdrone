﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone_Rotation : MonoBehaviour
{
    public bool usingController;
    public int playerNumber;

    public float friction;
    public int maxTilt;

    public float scopedAimMultiplier;
    public float xSensitivity;
    public float controllerAimMultiplier;

    public GameObject gunBarrelMesh;

    void Start()
    {
        usingController = GetComponentInParent<Drone_Stats>().usingController;
        playerNumber = GetComponentInParent<Drone_Stats>().playerNumber;

        friction = GetComponentInParent<Drone_Stats>().friction;
        maxTilt = GetComponentInParent<Drone_Stats>().maxTilt;
        
        xSensitivity = GetComponentInParent<Player_Settings>().xSensitivity;
        controllerAimMultiplier = GetComponentInParent<Player_Settings>().controllerAimMultiplier;

        gameObject.layer = playerNumber + 8;
        gunBarrelMesh.layer = playerNumber + 8;
        foreach (Transform child in gameObject.GetComponentsInChildren<Transform>())
        {
            child.gameObject.layer = gameObject.layer;
        }
    }
    
    void FixedUpdate()
    {
        Pitch();
        Roll();
        Yaw();
    }

    void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(new Vector3(tiltAmountForward, currentYRotation, -tiltAmountSidewards));
    }

    private float tiltAmountForward;
    void Pitch()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Pitch") != 0)
            {
                tiltAmountForward = Mathf.LerpAngle(tiltAmountForward, maxTilt * Input.GetAxis("p1_PC_Pitch"), 0.1f);
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_Controller_Pitch") != 0)
            {
                tiltAmountForward = Mathf.SmoothStep(tiltAmountForward, maxTilt * Input.GetAxis("p" + playerNumber + "_Controller_Pitch"), 0.1f);
            }
        }
    }

    private float tiltAmountSidewards;
    void Roll()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Roll") != 0)
            {
                tiltAmountSidewards = Mathf.SmoothStep(tiltAmountSidewards, maxTilt * Input.GetAxis("p1_PC_Roll"), 0.1f);
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_Controller_Roll") != 0)
            {
                tiltAmountSidewards = Mathf.SmoothStep(tiltAmountSidewards, maxTilt * Input.GetAxis("p" + playerNumber + "_Controller_Roll"), 0.1f);
            }
        }
    }

    private float wantedYRotation;
    private float currentYRotation;
    private float rotationYVelocity;
    void Yaw()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * Input.GetAxis("p1_PC_Yaw"));
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * controllerAimMultiplier * Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw"));
            }
        }
        currentYRotation = Mathf.SmoothDamp(currentYRotation, wantedYRotation, ref rotationYVelocity, 0.01f);
    }
}