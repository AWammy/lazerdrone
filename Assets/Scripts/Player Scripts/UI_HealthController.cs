﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HealthController : MonoBehaviour
{
    
    public void updateHealthValue(int value)
    {
        GetComponent<Slider>().value = value;
    }
}