﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone_Transform : MonoBehaviour
{
    private Rigidbody drone;
    public AudioSource hover;

    public bool usingController;
    public int playerNumber;

    public bool dead;
    public float elevationSpeed;
    public float baseMovementSpeed;
    public float movementSpeed;
    public float curBoost;
    public float boostSpeed;
    public float maxSpeed;
    public float friction;
    public bool boostAvailable;
    public bool boosting;

    public float scopedAimMultiplier;
    public float xSensitivity;
    public float controllerAimMultiplier;
    private float wantedYRotation;
    private float currentYRotation;

	//private UI_OutOfBoost outOfBoostUI;

    void Start()
    {
        drone = GetComponent<Rigidbody>();
        hover = GetComponent<AudioSource>();

        usingController = GetComponent<Drone_Stats>().usingController;
        playerNumber = GetComponent<Drone_Stats>().playerNumber;

        elevationSpeed = GetComponent<Drone_Stats>().elevationSpeed;
        baseMovementSpeed = GetComponent<Drone_Stats>().movementSpeed;
        boostSpeed = GetComponent<Drone_Stats>().boostSpeed;
        maxSpeed = GetComponent<Drone_Stats>().maxSpeed;
        friction = GetComponent<Drone_Stats>().friction;
        
        xSensitivity = GetComponent<Player_Settings>().xSensitivity;
        controllerAimMultiplier = GetComponent<Player_Settings>().controllerAimMultiplier;

		//outOfBoostUI = GetComponentInChildren<UI_OutOfBoost>();
    }
    
    void FixedUpdate()
    {
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, wantedYRotation, transform.rotation.z));

        AudioPitch();
        Boost();
        Elevation();
        Pitch();
        Roll();
        Yaw();
        MaxMovementSpeed();
        
        drone.velocity = drone.velocity * friction;
    }

    float maxPitch;
    void AudioPitch()
    {
        float totalVelocity = ((drone.velocity.x * drone.velocity.x) + (drone.velocity.y * drone.velocity.y) + (drone.velocity.z * drone.velocity.z)) / 60;
        if (totalVelocity < 1)
        {
            totalVelocity = 1;
        }
        if (boosting)
        {
            maxPitch = 3;
        }
        else if (!boosting)
        {
            maxPitch = 2;
        }
        if (totalVelocity > maxPitch)
        {
            totalVelocity = maxPitch;
        }
        hover.pitch = totalVelocity;
    }

    void Boost()
    {
		curBoost = GetComponent<Drone_Stats>().getBoost();;

        if (dead == true)
        {
            movementSpeed = baseMovementSpeed + boostSpeed;
            hover.mute = true;
        }
        else if (dead == false)
        {
            hover.mute = false;
            if (curBoost <= 0)
            {
                boostAvailable = false;
				//outOfBoostUI.setBoostGraphicColour(false);

            }
            if (curBoost >= 200)
            {
                boostAvailable = true;
				//outOfBoostUI.setBoostGraphicColour(true);
			}
            if (usingController == false)
            {
                if (Input.GetAxis("p1_PC_Boost") != 0 && boostAvailable == true)
                {
                    UsingBoost();
                }
                else if (Input.GetAxis("p1_PC_Boost") == 0 || boostAvailable == false)
                {
                    NotUsingBoost();
                }
            }
            else if (usingController == true)
            {
                if (Input.GetAxis("p" + playerNumber + "_Controller_Boost") != 0 && boostAvailable == true)
                {
                    UsingBoost();
                }
                else if (Input.GetAxis("p" + playerNumber + "_Controller_Boost") == 0 || boostAvailable == false)
                {
                    NotUsingBoost();
                }
            }
        }
    }

    void NotUsingBoost()
    {
        movementSpeed = baseMovementSpeed;
        boosting = false;
    }

    void UsingBoost()
    {
        movementSpeed = baseMovementSpeed + boostSpeed;
        GetComponent<Drone_Stats>().updateBoostValues(-1);
        boosting = true;
    }

    void Elevation()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Elevation") != 0)
            {
                drone.AddRelativeForce(Vector3.up * Input.GetAxis("p1_PC_Elevation") * movementSpeed);
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_Controller_Elevation") != 0)
            {
                print("elevation");
                drone.AddRelativeForce(Vector3.up * Input.GetAxis("p" + playerNumber + "_Controller_Elevation") * movementSpeed);
            }
        }
    }

    void Pitch()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Pitch") != 0)
            {
                drone.AddRelativeForce(Vector3.forward * Input.GetAxis("p1_PC_Pitch") * movementSpeed);
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_Controller_Pitch") != 0)
            {
                drone.AddRelativeForce(Vector3.forward * Input.GetAxis("p" + playerNumber + "_Controller_Pitch") * movementSpeed);
            }
        }
    }

    void Roll()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Roll") != 0)
            {
                drone.AddRelativeForce(Vector3.right * Input.GetAxis("p1_PC_Roll") * movementSpeed);
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_Controller_Roll") != 0)
            {
                drone.AddRelativeForce(Vector3.right * Input.GetAxis("p" + playerNumber + "_Controller_Roll") * movementSpeed);
            }
        }
    }

    void Yaw()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * Input.GetAxis("p1_PC_Yaw"));
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * controllerAimMultiplier * Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw"));
            }
        }
    }

    void MaxMovementSpeed()
    {
        if (drone.velocity.magnitude > maxSpeed)
        {
            drone.velocity = Vector3.ClampMagnitude(drone.velocity, maxSpeed);
        }
    }
}