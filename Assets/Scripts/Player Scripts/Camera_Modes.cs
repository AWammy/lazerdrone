﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Modes : MonoBehaviour
{

    public bool usingController;
    public int playerNumber;

    public GameObject lr;
    private Camera cam;
    private Transform trans;
    public float reticle;
    public Component[] scriptArr;

    public GameObject droneTrans;
    public GameObject droneRotate;
    public GameObject gunRotate;

    public float scopedAimMultiplier;
    public bool firstPerson;
    public int firstFOV;
    public int thirdFOV;
    public int scopedFOV;
    public Vector3 thirdPerson;

    void Start()
    {
        usingController = GetComponentInParent<Drone_Stats>().usingController;
        playerNumber = GetComponentInParent<Drone_Stats>().playerNumber;
        
        lr.layer = 0;
        cam = GetComponent<Camera>();
        trans = GetComponent<Transform>();
        reticle = GetComponentInChildren<RectTransform>().rotation.z;
        scopedAimMultiplier = GetComponentInParent<Player_Settings>().scopedAimMultiplier;
        if (firstPerson == true)
        {
            FirstPerson();
        }
        else if (firstPerson == false)
        {
            ThirdPerson();
        }
    }
    void Update()
    {
        if (usingController == false)
        {
            if (Input.GetButtonDown("p1_PC_Scope"))
            {
                Scoped();
            }               //use scope
            if (Input.GetButtonUp("p1_PC_Scope"))
            {
                if (firstPerson == true)
                {
                    FirstPerson();
                }
                else if (firstPerson == false)
                {
                    ThirdPerson();
                }
            }                 //unscope
            if (Input.GetButtonDown("p1_PC_SwitchPerspective"))
            {
                //print("PRESSED C");
                if (firstPerson == true)
                {
                    firstPerson = false;
                    ThirdPerson();
                }
                else if (firstPerson == false)
                {
                    firstPerson = true;
                    FirstPerson();
                }
            }   //switch mode
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Scope") > 0)
            {
                //Debug.Log(Input.GetJoystickNames()[playerNumber - 1] + playerNumber + " is scoped");
                Scoped();
            }
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Scope") == 0)
            {
                //Debug.Log(Input.GetJoystickNames()[playerNumber - 1] + playerNumber + " is unscoped");
                if (firstPerson == true)
                {
                    FirstPerson();
                }
                else if (firstPerson == false)
                {
                    ThirdPerson();
                }
            }
            if (Input.GetButtonDown("p" + playerNumber + "_Controller_SwitchPerspective"))
            {
                if (firstPerson == true)
                {
                    firstPerson = false;
                    ThirdPerson();
                }
                else if (firstPerson == false)
                {
                    firstPerson = true;
                    FirstPerson();
                }
            }
        }
    }
    void FirstPerson()
    {
        cam.fieldOfView = firstFOV;
        cam.cullingMask = ~(1 << playerNumber + 8);
        trans.localPosition = new Vector3(0, 0.1f , 0);
        Mathf.Lerp(reticle, 0, 0.5f);
        Scoping(0.85f, 1);

    }
    void ThirdPerson()
    {
        cam.fieldOfView = thirdFOV;
        trans.localPosition = new Vector3(thirdPerson.x, thirdPerson.y, thirdPerson.z);
        cam.cullingMask |= (1 << playerNumber + 8);
        Mathf.Lerp(reticle, 0, 0.5f);
        Scoping(0.85f, 1);
    }
    void Scoped()
    {
        cam.fieldOfView = scopedFOV;
        cam.cullingMask = ~(1 << playerNumber + 8);
        trans.localPosition = new Vector3(0, 0.1f , 0);
        trans.localRotation = new Quaternion(0, 0, 0, 0);
        Mathf.Lerp(reticle, 135, 0.5f);
        Scoping(0.5f, scopedAimMultiplier);
    }
    private void Scoping(float slow, float aDS)
    {
        droneTrans.GetComponent<Drone_Transform>().friction = slow;
        droneTrans.GetComponent<Drone_Transform>().scopedAimMultiplier = aDS;
        droneRotate.GetComponent<Drone_Rotation>().scopedAimMultiplier = aDS;
        gunRotate.GetComponent<Gun_Rotation>().scopedAimMultiplier = aDS;
        /*for (int i=0; i<3; i++)
        {
            GetComponentInParent<scriptArr[i]>().scopedAimMultiplier;
        }*/
    }
}