﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_OutOfBoost : MonoBehaviour {

    public Graphic m_Graphic;
    public Color m_MyColor;

    void Start()
    {
		m_Graphic = GetComponent<Graphic>();

        m_MyColor = Color.yellow;

        m_Graphic.color = m_MyColor;
    }
    
	public void setBoostGraphicColour(bool boost) 
    {
	    if (!boost)
        {
            m_MyColor = Color.Lerp(Color.yellow, Color.red, Mathf.PingPong(Time.time, 0.5f));
        }
        else
        {
            m_MyColor = Color.yellow;
        }
        m_Graphic.color = m_MyColor;
	}
}
