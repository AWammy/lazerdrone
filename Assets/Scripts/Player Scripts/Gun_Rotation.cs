﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun_Rotation : MonoBehaviour
{
    public bool usingController;
    public int playerNumber;

    public int maxVerticleAim;
    public float scopedAimMultiplier;
    public float xSensitivity;
    public float ySensitivity;
    public float controllerAimMultiplier;

    void Start()
    {
        usingController = GetComponentInParent<Drone_Stats>().usingController;
        playerNumber = GetComponentInParent<Drone_Stats>().playerNumber;

        maxVerticleAim = GetComponentInParent<Drone_Stats>().maxVerticleAim;
        
        xSensitivity = GetComponentInParent<Player_Settings>().xSensitivity;
        ySensitivity = GetComponentInParent<Player_Settings>().ySensitivity;
        controllerAimMultiplier = GetComponentInParent<Player_Settings>().controllerAimMultiplier;
    }
    void FixedUpdate()
    {
        Yaw();
        AimVertically();
        MaxRotation();

        transform.rotation = Quaternion.Euler(new Vector3(currentXRotation, currentYRotation, transform.rotation.z));
    }

    private float wantedYRotation;
    public float currentYRotation;
    private float rotationYVelocity;
    void Yaw()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * Input.GetAxis("p1_PC_Yaw"));
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw") != 0)
            {
                wantedYRotation = currentYRotation += (scopedAimMultiplier * xSensitivity * controllerAimMultiplier * Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Yaw"));
            }
        }
        currentYRotation = Mathf.SmoothDamp(currentYRotation, wantedYRotation, ref rotationYVelocity, 0.01f);
    }

    private float wantedXRotation;
    public float currentXRotation;
    private float rotationXVelocity;
    void AimVertically()
    {
        if (usingController == false)
        {
            if (Input.GetAxis("p1_PC_Aim") != 0)
            {
                wantedXRotation = currentXRotation -= (scopedAimMultiplier * ySensitivity * Input.GetAxis("p1_PC_Aim"));
            }
        }
        else if (usingController == true)
        {
            if (Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Aim") != 0)
            {
                wantedXRotation = currentXRotation -= (scopedAimMultiplier * ySensitivity * controllerAimMultiplier * Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Aim"));
            }
        }
        currentXRotation = Mathf.SmoothDamp(currentXRotation, wantedXRotation, ref rotationXVelocity, 0.25f);
    }

    void MaxRotation()
    {
        if (currentXRotation > maxVerticleAim)
        {
            currentXRotation = Mathf.Clamp(currentXRotation, maxVerticleAim, maxVerticleAim);
        }
        else if (currentXRotation < -maxVerticleAim)
        {
            currentXRotation = Mathf.Clamp(currentXRotation, -maxVerticleAim, -maxVerticleAim);
        }
    }
}