﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone_Stats : MonoBehaviour
{

    //Player Setup Variables
    public bool usingController;    //Determines whether an XBOX 360 controller or a Keyboard and Mouse is used
    public int playerNumber;        //Player Controller Number (not relevant if usingController is disabled)

    //Drone Control Variables
    public float elevationSpeed;    //Y Transform
    public float movementSpeed;     //XZ Transform
    public float boostSpeed;
    public float maxSpeed;          //Maximum Drone Transform Velocity
    public float friction;          //The number that a Drone's Transform Velocity will be exponentially decreased (0 = Total Friction, 1 = No Friction)
    public float baseFriction;      //The number that a Drone's Transform Velocity will be exponentially decreased (0 = Total Friction, 1 = No Friction)
    public float scopedFriction;    //The number that a Drone's Transform Velocity will be exponentially decreased whilst scoping (0 = Total Friction, 1 = No Friction)
    public int maxTilt;             //Maximum Drone XZ Rotation
    public int maxVerticleAim;      //Maximum Drone Gun X Rotation

    //Combat Variables
    public int maxHealth;
    private int curHealth;			//all changes to this value will happen inside this script - see methods at end
    public int regenHealth;
    public float projectileSpeed;   //Speed Drone Gun projectiles will be spawned at
    public int maxBoost;
    public float curBoost;
    public int regenBoost;
    public bool dead;
    public int damagePerHit;
    public int hitRange;
    public float attackSpeed;
    public float respawnTimer;

	//WHICH ONE OFTHESE ARE YOU USING
	//private UI_Controller uiController;
	private UI_HealthController uiHealthController;

	private UI_BoostController uiBoostController;

	private float timer;

	private void Awake()
    {
        curHealth = maxHealth;
        dead = false;
        baseFriction = friction;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

	private void Start()
	{
		//uiController = GetComponentInChildren<UI_Controller>();
		uiHealthController = GetComponentInChildren<UI_HealthController>();
        uiBoostController = GetComponentInChildren<UI_BoostController>();
		uiHealthController.updateHealthValue(maxHealth);
	}


	private void Update()
    {
        respawnTimer -= Time.deltaTime;
        timer += Time.deltaTime;
        if (timer >= 0.1f)
        {
			updateHealthValue(-regenHealth); //note the negative value as currently updateHV deletes the value passed
            updateBoostValues(regenBoost);
            timer -= 0.1f;
        }
        
        
        
        if (dead == true && respawnTimer <= 0f)
        {
            dead = false;
			updateHealthValue(-maxHealth*2); //updateHV will reset this to  maxHealth as adding 2*maxHealth onto a negative number once dead
        }
    }


    void Death(int playerNumber)
    {
        dead = true;
        Debug.Log("player died");
        GameObject.Find("GameController").GetComponent<GameMode_Controller>().deaths[playerNumber -= 1] += 1;
        respawnTimer = 5f;
    }

	//this will be called at all places where health is currently changing
	//finding all references for curHealth as a search is quickest way to locate these
	//any call that relies on a change in value of curHealth can be placed here now rather than in an expensive Update
	public void updateHealthValue(int value)
	{
		curHealth -= value;

		if (curHealth > maxHealth)
		{
			curHealth = maxHealth;
		}

		if (curHealth <= 0 && dead == false)
		{
			Death(playerNumber);
		}

		//which of these are you using
		//uiController.setHealthSlider(curHealth);
		uiHealthController.updateHealthValue(curHealth);

	}

	public void updateBoostValues(int value)
	{
		curBoost += value;

		if (curBoost > maxBoost)
		{
			curBoost = maxBoost;
		}

		uiBoostController.updateBoostSlider(curBoost);
	}

	public int getCurrentHealth()
	{
		return curHealth;
	}

	public float getBoost()
	{
		return curBoost;
	}
}