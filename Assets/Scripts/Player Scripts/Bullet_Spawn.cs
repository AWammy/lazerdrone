﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Spawn : MonoBehaviour
{
    #region //Variables
    public Rigidbody projectile;

    public bool usingController;
    public int playerNumber;

    AudioSource shootSound;
    #endregion

    void Start() //Populating Variables
    {
        usingController = GetComponentInParent<Drone_Stats>().usingController;
        playerNumber = GetComponentInParent<Drone_Stats>().playerNumber;

        shootSound = GetComponent<AudioSource>();
    }

    void Update()
    {
        Fire();
    }

    void Fire()
    {
        if (usingController == false && Input.GetButton("p1_PC_Fire") && shootSound.isPlaying == false)
        {
            //shootSound.Play();
            //Rigidbody instance = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
        } //PC shoot
        else if (usingController == true && Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Fire") > 0)
        {
            //Rigidbody instance = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
            //shootSound.Play();
        } //Controller shoot
    }
}