﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//currently this script appears unused as did a search for Drone_Death and this appears as the only reference
//but having the code in FixedUpdate causes unneccessary calls. If the objective is to change the active status of
//the body and gun whne the value change sin Drone_Stats then have call from that script that calls a method in this 
//script when that value changes

public class Drone_Death : MonoBehaviour
{

    public GameObject Body;
    public GameObject Gun;

    public bool dead;
    
	/*void FixedUpdate ()
    {
        dead = GetComponentInParent<Drone_Stats>().dead;
        Body.SetActive(!dead);
        Gun.SetActive(!dead);
    }*/

	 public void updateActiveStatus(bool val)
	{
		Body.SetActive(val);
		Gun.SetActive(val);
	}
}
