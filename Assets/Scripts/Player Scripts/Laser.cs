﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public bool usingController;
    public int playerNumber;
    private IEnumerator coroutine;
    public GameObject barrelGO;
    private Vector3 barrelTransform;
    public LineRenderer lineRenderer;
    public GameObject decal;
    public AudioSource shootSound;
    public AudioSource hitSound;
    public GameObject hitMarker;
    public int damagePerHit;
    public int damageDealt;
    public float hitRange;
    public float maxHitRange;
    public float attackSpeed;
    public float laserFadeSpeed;

    void Start()
    {
        usingController = GetComponentInParent<Drone_Stats>().usingController;
        playerNumber = GetComponentInParent<Drone_Stats>().playerNumber;
        
        lineRenderer.enabled = false;
        damagePerHit = GetComponentInParent<Drone_Stats>().damagePerHit;
        maxHitRange = GetComponentInParent<Drone_Stats>().hitRange;
        attackSpeed = GetComponentInParent<Drone_Stats>().attackSpeed;
        
        if(attackSpeed <= 0)
        {
            attackSpeed = 1;
        }
        if(laserFadeSpeed < 0)
        {
            laserFadeSpeed = 0.5f;
        }
        if (laserFadeSpeed > 1)
        {
            laserFadeSpeed = 1f;
        }
        shootSound.pitch = attackSpeed;
        hitSound.pitch = attackSpeed;
    }

    void Update()
    {
        barrelTransform = barrelGO.transform.position;
        hitRange = maxHitRange;
        if (usingController == false && Input.GetButton("p1_PC_Fire") && shootSound.isPlaying == false)
        {
            Shoot();
        }
        else if (usingController == true && Mathf.Abs(Input.GetAxis("p" + playerNumber + "_ControllerXBONE_Fire")) > 0 && shootSound.isPlaying == false)
        {
            //Debug.Log(Input.GetJoystickNames()[playerNumber - 1] + playerNumber + " is shooting");
            Shoot();
        }
        lineRenderer.SetPosition(0, barrelTransform);
        lineRenderer.SetPosition(1, hit.point);
        lineRenderer.startWidth = lineRenderer.startWidth * laserFadeSpeed;
    }

    RaycastHit hit;
    void Shoot()
    {
        shootSound.Play();
        lineRenderer.enabled = true;
        if (Physics.Raycast(transform.position, transform.forward, out hit, hitRange))
        {
            if (hit.collider.gameObject.tag == "Player")
            {
                damageDealt = damagePerHit;
                if (hit.collider.gameObject.GetComponentInParent<Drone_Stats>().getCurrentHealth() <= damageDealt)
                {
                    playerKill(playerNumber);
                }
                playerHit(playerNumber, damageDealt);
                hit.collider.gameObject.GetComponentInParent<Drone_Stats>().updateHealthValue(damageDealt);
                hitRange = hit.distance;
                hitSound.PlayDelayed(0.594f / attackSpeed);
            }
            if (hit.collider.gameObject.tag == "Crit")
            {
                damageDealt = damagePerHit * 2;
                if(hit.collider.gameObject.GetComponentInParent<Drone_Stats>().getCurrentHealth()<= damageDealt)
                {
                    playerKill(playerNumber);
                }
                playerHit(playerNumber, damageDealt);
                hit.collider.gameObject.GetComponentInParent<Drone_Stats>().updateHealthValue(damageDealt);
                hitRange = hit.distance;
                hitSound.PlayDelayed(0.594f / attackSpeed);
            }
            if (hit.collider.gameObject.tag == "Environment")
            {
                Instantiate(decal, hit.point, Quaternion.identity);
                hitRange = hit.distance;
            }
        }
        lineRenderer.startWidth = 0.1f;
    }
    void playerHit(int playerNumber, int damageDealt)
    {
        GameObject.Find("GameController").GetComponent<GameMode_Controller>().score[playerNumber -= 1] += damageDealt;
        StartCoroutine("HitMarker", 0.25f);
        StopCoroutine("HitMarker");
    }
    void playerKill(int playerNumber)
    {
        GameObject.Find("GameController").GetComponent<GameMode_Controller>().kills[playerNumber -= 1] += 1;
    }

    IEnumerator HitMarker(float timer)
    {
        while (true)
        {
            hitMarker.SetActive(true);
        }
    }
}