﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BoostController : MonoBehaviour
{
    
    public void updateBoostSlider(float value)
    {
        GetComponent<Slider>().value = value;
    }
}