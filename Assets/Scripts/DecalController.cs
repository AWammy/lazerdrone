﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalController : MonoBehaviour {
    
    public float destroyTime;

	/*Switched this from Update to Start - Update will be called every frame so Unity will be storing
	 * a reference to Destroy this object in all the frames uptil the point of the object being destroyed
	 * if this was for example 5secs then at 60fps that would be 299 unneccessary calls. These would all need to 
	 * be tidied up when the object is eventually destroyed
	 */

	void Start ()
    {
        Destroy(gameObject, destroyTime);
    }
}