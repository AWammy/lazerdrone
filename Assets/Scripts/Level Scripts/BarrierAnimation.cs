﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierAnimation : MonoBehaviour
{
    public int ticksPerSecond;
    public float pingPongSpeed;
    public Vector2 startOffset;
    private Vector2 scrollStep;
    public Vector2 scrollSpeed;
    public bool pingPongAnimation;
    public Vector2 pingPongSize;
    public Vector2 offset;
    private float tickTimer, timer;

    void Start()
    {
        tickTimer = (1f / ticksPerSecond);
        offset.x = startOffset.x;
        offset.y = startOffset.y;
    }

	void Update ()
    {
        timer -= Time.deltaTime;

        if (pingPongAnimation == false && timer <= 0)
        {
            constantAnimation();
            timer = tickTimer;
        }
        else if (pingPongAnimation == true && timer <= 0)
        {
            pingPong();
            timer = tickTimer;
        }
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset.x, offset.y));
    }

    void constantAnimation()
    {
        offset.x += scrollSpeed.x;
        offset.y += scrollSpeed.y;
        if (offset.x >= 1)
        {
            offset.x--;
        }
        else if (offset.y >= 1)
        {
            offset.y--;
        }
    }

    void pingPong()
    {
        //scrollStep.x = Mathf.PingPong(Time.deltaTime, pingPongSize.x);// * 2 - pingPongSize.x);
        //scrollStep.y = Mathf.PingPong(Time.deltaTime, pingPongSize.y);// * 2 -pingPongSize.y);
        //offset.x += scrollStep.x;
        //offset.y += scrollStep.y;
        scrollSpeed.x = Mathf.PingPong(Time.time * pingPongSpeed, pingPongSize.x * 2) - pingPongSize.x;
        scrollSpeed.y = Mathf.PingPong(Time.time * pingPongSpeed, pingPongSize.y * 2) - pingPongSize.y;
        offset.x += scrollSpeed.x;
        offset.y += scrollSpeed.y;
    }
}
