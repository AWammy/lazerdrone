﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostRing : MonoBehaviour {

    public int boostGained = 200;
    public bool boostCharged;
    public float ringCooldown = 15;
    public float timeToCharged;

	void Start ()
    {
        boostCharged = true;
        timeToCharged = ringCooldown;	
	}
	
	void Update ()
    {
        if (boostCharged == false)
        {
            timeToCharged -= Time.deltaTime;
            if (timeToCharged <= 0)
            {
                boostCharged = true;
                timeToCharged = ringCooldown;
            }
        }
	}
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Player" && boostCharged == true)
        {
            other.gameObject.GetComponentInParent<Drone_Stats>().updateBoostValues(boostGained);
            boostCharged = false;
        }
    }
}
