﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*removed the fixed update and replaced with StartCoroutine
 * to be honest - not sure whether this will run faster
 * BUT I would also consider having this one script control all the BUOYS
 * and have list object store each buoy and then in the coroutine traverse the list changing 
 * all on / off. This would require moving all the scripts off the flare object and having
 * one buoy controller
 * */

public class Buoys_Flashing : MonoBehaviour {

    //public float timer;
    public int flashRate;

	private LensFlare lensflare;
	private Light light;

	private void Start()
	{
		lensflare = GetComponent<LensFlare>();
		light = GetComponent<Light>();

		lensflare.enabled = false;
		light.enabled = false;

		StartCoroutine(switchLightOnOff());
	}

	private IEnumerator switchLightOnOff()
	{
		lensflare.enabled = !lensflare.enabled;
		light.enabled = !light.enabled;
		yield return new WaitForSeconds(flashRate);

		StartCoroutine(switchLightOnOff());
	}
}
