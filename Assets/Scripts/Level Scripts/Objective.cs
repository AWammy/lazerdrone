﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour
{
    private int[] leaderboard;

    private int playersInObjective;
    private float pointScoreTimer;
    public int pointsPerSecond;
    private int playerNumberWorkAround; //I feel like this the best way to work around my scripting problem

    void Start()
    {
        playerNumberWorkAround = -1;
        leaderboard = GameObject.Find("GameController").GetComponent<GameMode_Controller>().score;
        playersInObjective = 0;
    }
    
	void Update ()
    {
        pointScoreTimer -= Time.deltaTime;
        countPlayers();
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            playerNumberWorkAround += other.gameObject.GetComponentInParent<Drone_Stats>().playerNumber;
            playersInObjective += 1;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerNumberWorkAround -= other.gameObject.GetComponentInParent<Drone_Stats>().playerNumber;
            playersInObjective -= 1;
        }
    }

    void countPlayers()
    {
        if (playersInObjective == 0)
        {
            //set colour to white
            pointScoreTimer = 1;
        }
        if (playersInObjective == 1 && pointScoreTimer <= 0)
        {
            pointScoreTimer = 1;
            pointScored(playerNumberWorkAround, pointsPerSecond);
        }
        if (playersInObjective > 1)
        {
            //set colour to red
            pointScoreTimer = 1;
        }
    }

    void pointScored(int playerNumberWorkAround, int pointsPerSecond)
    {
        leaderboard[playerNumberWorkAround] += pointsPerSecond;
    }
}