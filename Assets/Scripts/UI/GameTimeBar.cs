﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimeBar : MonoBehaviour {

    public float startTime;

	//class instance varaiable to save repeated calls + Find(byName) is an expensive call
	private Slider slider;
	private GameMode_Controller controller;
	
    void Start()
    {
		slider = GetComponent<Slider>();
		controller = GameObject.Find("GameController").GetComponent<GameMode_Controller>();

		startTime = controller.gameTimeInSeconds;
        slider.value = 1;
    }

	void Update ()
    {
        slider.value = controller.gameTimeInSeconds / startTime;
	}
}
