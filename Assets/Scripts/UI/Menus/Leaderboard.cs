﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public GameObject[] playerSlots;

   	void Start ()
    {
        for(int j = 0; j < GameObject.Find("EventSystem").GetComponent<DontDestroy>().playMode; j++)
        {//if errors occur with the leaderboard ensure that DontDestroy.playmode is behaving correctly and that GameObjects in PlayerSlots are named correctly
            playerSlots[j].SetActive(true); //this line works
            GameObject.Find("TotalScore" + (j + 1)).GetComponent<Text>().text = GameObject.Find("EventSystem").GetComponent<DontDestroy>().endTotalScore[j].ToString();
            GameObject.Find("Damage" + (j + 1)).GetComponent<Text>().text = GameObject.Find("EventSystem").GetComponent<DontDestroy>().endScores[j].ToString();
            GameObject.Find("Kills" + (j + 1)).GetComponent<Text>().text = GameObject.Find("EventSystem").GetComponent<DontDestroy>().endKills[j].ToString();
            GameObject.Find("Deaths" + (j + 1)).GetComponent<Text>().text = GameObject.Find("EventSystem").GetComponent<DontDestroy>().endDeaths[j].ToString();
        }
	}
}
