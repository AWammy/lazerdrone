﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningCameraEffect : MonoBehaviour
{
    private float rotateX;
    private float rotateY;
    private float rotateZ;
    public float spinRate;
    
    void Update()
    {
        rotateX += spinRate * Time.deltaTime;
        rotateY += spinRate * Time.deltaTime;
        rotateZ += spinRate * Time.deltaTime;
        
        GetComponent<Transform>().localEulerAngles = new Vector3(rotateX, rotateY, rotateZ);
    }
}
