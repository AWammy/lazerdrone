﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Canvas MainCanvas;
    public enum Scene { start, mainMenu, controlsCredits, gameSetup, setup, play, end };
    public Scene sceneMode;
    private int mainMenuSelection;
    public float inputDelay = 0.15f;
    private float inputTimer;
    public int mapSelection;
    public int playerSelection;

    private void Update()
    {
        #region //Start Scene Controls
        if (sceneMode == Scene.start) 
        {
            //if(Input.GetAxis("Controller_A") != 0)
            if(Input.anyKey)
            {
                LoadSplitScreen();
            }
        }
        #endregion
        #region //mainMenu Scene Controls
        if (sceneMode == Scene.mainMenu)
        {
            inputTimer -= Time.deltaTime;
            if (Input.GetAxis("Controller_A") != 0)
            {
                SceneSelected(mainMenuSelection);
            }
            if (Input.GetAxis("Controller_DPad_Vertical") > 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(mainMenuSelection, -1);
            }
            if (Input.GetAxis("Controller_DPad_Vertical") < 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(mainMenuSelection, 1);
            }
            switch (mainMenuSelection)
            {
                case 1://GameSetup
                    Highlighter(0);
                    break;
                case 2://Controls
                    Highlighter(-50);
                    break;
                case 3://Credits
                    Highlighter(-100);
                    break;
                case 4://Quit
                    Highlighter(-150);
                    break;
            }
            if (Input.GetAxis("Cancel") != 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);

                sceneMode = Scene.start;
            }
        }
        #endregion
        #region //controlsCredits Scene Controls
        if (sceneMode == Scene.controlsCredits)
        {
            if (Input.GetAxis("Cancel") != 0)
            {
                sceneMode = Scene.mainMenu;
            }
        }
        #endregion
        #region //gameSetup Scene Controls
        if (sceneMode == Scene.gameSetup)
        {
            if (Input.GetAxis("Controller_DPad_Vertical") >0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(playerSelection, -1);
            }
            if (Input.GetAxis("Controller_DPad_Vertical") <0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(playerSelection, 1);
            }
            if (Input.GetAxis("Controller_DPad_Horizontal") > 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(mapSelection, -1);
            }
            if (Input.GetAxis("Controller_DPad_Horizontal") < 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                Selection(mapSelection, 1);
            }
        }
        #endregion
        #region //Setup Scene Controls
        if (sceneMode == Scene.setup)
        {
            inputTimer -= Time.deltaTime;
            if(Input.GetAxis("Controller_X") != 0)
            {
                Load2Player();
                LoadPTS();
            }
            if (Input.GetAxis("Controller_A") != 0)
            {
                Load3Player();
                LoadPTS();
            }
            if (Input.GetAxis("Controller_B") != 0)
            {
                Load4Player();
                LoadPTS();
            }
            if (Input.GetAxis("Controller_Y") != 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                SetP1Toggle();
            }
            if (Input.GetAxis("Cancel") != 0 && inputTimer <= 0)
            {
                InputTimer(inputDelay);
                LoadStart();
            }
        }
        #endregion
        #region //Play Scene Controls
        if (sceneMode == Scene.play)
        {
            if (Input.GetAxis("Controller_A") !=0 && Input.GetAxis("Controller_B") !=0 && Input.GetAxis("Controller_X") !=0 && Input.GetAxis("Controller_Y") != 0)
            {
                LoadPostGame();
            }
            if (Input.GetAxis("Cancel") != 0)
            {
                LoadSplitScreen();
            }
        }
        #endregion
        #region //End Scene Controls
        if (sceneMode == Scene.end)
            {
                if(Input.GetAxis("Controller_X") != 0)
                {
                    GameObject.Find("XButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                    LoadPTS();
                }
                if(Input.GetAxis("Controller_A") != 0)
                {
                    GameObject.Find("AButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                    LoadSplitScreen();
                }
                if(Input.GetAxis("Controller_B") != 0)
                {
                    GameObject.Find("BButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
                    QuitGame();
                }
                if (Input.GetAxis("Cancel") != 0)
                {
                   LoadSplitScreen();
                }
        }
        #endregion
    }
    public void SceneSelected(int caseNumber)
    {
        switch (caseNumber)
        {
            case 1://GameSetup
                GameObject.Find("Line").GetComponent<LineRenderer>().enabled = true;
                SceneManager.LoadScene("GameSetup");
                break;
            case 2://Controls
                GameObject.Find("Line").GetComponent<LineRenderer>().enabled = true;
                SceneManager.LoadScene("Controls");
                break;
            case 3://Credits
                GameObject.Find("Line").GetComponent<LineRenderer>().enabled = true;
                SceneManager.LoadScene("Credits");
                break;
            case 4://Quit
                GameObject.Find("Line").GetComponent<LineRenderer>().enabled = true;
                QuitGame();
                break;
        }
    }
    public void Highlighter(int h)
    {
        GameObject.Find("Line").GetComponent<RectTransform>().localPosition = new Vector3(GameObject.Find("Line").GetComponent<RectTransform>().localPosition.x, h);
    }
    public void Selection(int selection, int updown)
    {
        selection += updown;
        if(mainMenuSelection < 1)
        {
            mainMenuSelection = 1;
        }
        if(mainMenuSelection > 4)
        {
            mainMenuSelection = 4;
        }
        if(mapSelection !=1)
        {
            mapSelection = 1;
        }
        if(playerSelection < 1)
        {
            playerSelection = 2;
        }
        if(playerSelection > 4)
        {
            playerSelection = 4;
        }
    }
    public void InputTimer(float timer)
    {
        inputTimer = timer;
    }
    public void SetP1Toggle()
    {
        if (GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController == true)
        {
            GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController = false;
            GameObject.Find("Y Icon").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
            GameObject.Find("Mouse Icon").GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
        else if (GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController == false)
        {
            GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController = true;
            GameObject.Find("Y Icon").GetComponent<Image>().color = new Color(1, 1, 1, 1);
            GameObject.Find("Mouse Icon").GetComponent<Image>().color = new Color(1, 1, 1, 0.1f);
        }
    }
    public void Load2Player()
    {
        GameObject.Find("2pButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        GameObject.Find("EventSystem").GetComponent<DontDestroy>().playMode = 2;
        LoadPTS();
    }
    public void Load3Player()
    {
        GameObject.Find("3pButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        GameObject.Find("EventSystem").GetComponent<DontDestroy>().playMode = 3;
        LoadPTS();
    }
    public void Load4Player()
    {
        GameObject.Find("4pButton").GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
        GameObject.Find("EventSystem").GetComponent<DontDestroy>().playMode = 4;
        LoadPTS();
    }
    public void LoadStart()
    {
        SceneManager.LoadScene("Start");
    }
    public void LoadSplitScreen()
    {
        SceneManager.LoadScene("SplitScreenSetUp");
    }
    public void LoadPTS()
    {
        SceneManager.LoadScene("PrivateTestScene");
    }
    public void LoadPostGame()
    {
        SceneManager.LoadScene("PostGame");
    }
    public void Cancel()
    {
        if (Input.GetAxis("Cancel") !=0)
        {
            
        }
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}