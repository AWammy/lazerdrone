﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressToStart : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetAxis("Controller_A") != 0)
        {
            LoadSplitScreen();
        }
    }
    private void LoadSplitScreen()
    {
        SceneManager.LoadScene("SplitScreenSetUp");
        print("LoadSplitScreen");
    }
}
