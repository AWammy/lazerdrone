﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject dronePrefab;
    public int numberOfPlayers = 1;

    public Vector3[] spawnPoint;
    public Camera[] cameras;
    public GameObject[] dronesArray;
   
    void Start()
    {
        numberOfPlayers = GameObject.Find("EventSystem").GetComponent<DontDestroy>().playMode;
        InstantiateDrones();
        ResetAllDrones();
        PCPlayer();
        SetUpCameras();
    }

    public void InstantiateDrones()
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            Instantiate(dronePrefab, spawnPoint[i], Quaternion.identity);
            /*if(i == numberOfPlayers - 1)
            {
                GameObject.FindGameObjectWithTag("PlayerParent").GetComponent<Drone_Stats>().usingController = GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController;
            }*/
        }
    }

    private void ResetAllDrones()
    {
        dronesArray = GameObject.FindGameObjectsWithTag("PlayerParent") as GameObject[];
        for (int i = 0; i < dronesArray.Length; i++)
            {
                dronesArray[i].GetComponent<Drone_Stats>().playerNumber = i + 1;
            }
        foreach (GameObject drone in dronesArray)
        {
            drone.SetActive(false);
        }
    }

    private void PCPlayer()
    {
        if(GameObject.Find("EventSystem").GetComponent<DontDestroy>().usingController == false)
        {
            dronesArray[numberOfPlayers - 1].GetComponent<Drone_Stats>().usingController = false;
        }
    }

    public void SetUpCameras()
    {
        cameras = new Camera[dronesArray.Length];
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i] = dronesArray[i].GetComponentInChildren<Camera>();
        }
        switch (numberOfPlayers)
        {
            case 1:
                dronesArray[0].SetActive(true);
                cameras[0].enabled = true;
                cameras[0].rect = new Rect(0, 0, 1f, 1f);
                break;
            case 2:
                dronesArray[0].SetActive(true);
                dronesArray[1].SetActive(true);
                cameras[0].enabled = true;
                cameras[1].enabled = true;
                cameras[0].rect = new Rect(0, 0.5f, 1, 0.5f);
                cameras[1].rect = new Rect(0, 0, 1, 0.5f);
                break;
            case 3:
                dronesArray[0].SetActive(true);
                dronesArray[1].SetActive(true);
                dronesArray[2].SetActive(true);
                cameras[0].enabled = true;
                cameras[1].enabled = true;
                cameras[2].enabled = true;
                cameras[0].rect = new Rect(0f, 0.5f, 1f, 0.5f);
                cameras[1].rect = new Rect(0f, 0f, 0.5f, 0.5f);
                cameras[2].rect = new Rect(0.5f, 0f, 0.5f, 0.5f);
                break;
            case 4:
                dronesArray[0].SetActive(true);
                dronesArray[1].SetActive(true);
                dronesArray[2].SetActive(true);
                dronesArray[3].SetActive(true);
                cameras[0].enabled = true;
                cameras[1].enabled = true;
                cameras[2].enabled = true;
                cameras[3].enabled = true;
                cameras[0].rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                cameras[1].rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                cameras[2].rect = new Rect(0f, 0f, 0.5f, 0.5f);
                cameras[3].rect = new Rect(0.5f, 0, 0.5f, 0.5f);
                break;
        }
    }
}