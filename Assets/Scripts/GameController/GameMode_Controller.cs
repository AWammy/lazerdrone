﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMode_Controller : MonoBehaviour
{
    #region //GameMode Variables
    public GameObject[] objectives;
    public float objectiveTimer;
    public int curObjectiveActive;
    private int newObjectiveActive;
    #endregion
    #region //Scoreboard Variables
    public int numberOfPlayers;
    public int[] score;
    public int[] kills;
    public int[] deaths;
    public bool gameEnded;
    
    public float gameTimeInSeconds;

    public GameObject eventSystem;
    #endregion
    void Start()
    {
        eventSystem = GameObject.Find("EventSystem");
        numberOfPlayers = eventSystem.GetComponent<DontDestroy>().playMode;
        ResetScoreboard();
        gameEnded = false;
        SetupGameMode();
    }
    public void Update() //Called once per frame
    {
        objectiveTimer -= Time.deltaTime;
        gameTimeInSeconds -= Time.deltaTime;
        if (0 >= gameTimeInSeconds && gameEnded == false)
        {
            gameEnded = true;
            GameOver();
        }

        if (Input.GetAxis("Controller_A") != 0 && Input.GetAxis("Controller_B") != 0 && Input.GetAxis("Controller_X") != 0 && Input.GetAxis("Controller_Y") != 0)
        {
            gameTimeInSeconds = 0;
        }

        ActivateObjective();
    }
    private void ResetScoreboard() //Called at Start
    {
        Array.Clear(score, 0, score.Length);
        Array.Clear(kills, 0, kills.Length);
        Array.Clear(deaths, 0, deaths.Length);
    }
    public void SetupGameMode() //Called at Start
    {
        objectives = GameObject.FindGameObjectsWithTag("Objective") as GameObject[];

        foreach (GameObject objective in objectives)
        {
            objective.SetActive(false);
        }
    }
    private void ActivateObjective()
    {
        if (objectiveTimer <= 0)
        {
            objectiveTimer = 30;
            ChooseNewObjective();
            objectives[curObjectiveActive].SetActive(false);
            objectives[newObjectiveActive].SetActive(true);
            curObjectiveActive = newObjectiveActive;
        }
    }
    private void ChooseNewObjective()
    {
        newObjectiveActive = UnityEngine.Random.Range(0, objectives.Length);
        if(newObjectiveActive == curObjectiveActive)
        {
            ChooseNewObjective();
        }
    }
    private void GameOver()
    {
        //Debug.Log("Game Over!");
        for (int i = 0; i < numberOfPlayers; i++)
        {
            eventSystem.GetComponent<DontDestroy>().endScores[i] = score[i];
            eventSystem.GetComponent<DontDestroy>().endKills[i] = kills[i];
            eventSystem.GetComponent<DontDestroy>().endDeaths[i] = deaths[i];
            eventSystem.GetComponent<DontDestroy>().endTotalScore[i] = (score[i] * (1 + kills[i]/1 + deaths[i]));
            Debug.Log("player " + (i + 1) + " scored " + (score[i] * (1 + kills[i]/1 + deaths[i])));
        }

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("PostGame");
    }
}