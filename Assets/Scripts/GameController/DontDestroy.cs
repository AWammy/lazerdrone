﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DontDestroy : MonoBehaviour
{
    public bool usingController;
    public int playMode;

    public int[] endScores;
    public int[] endKills;
    public int[] endDeaths;
    public int[] endTotalScore;

	void Awake ()
    {
        DontDestroyOnLoad(this.gameObject);
        usingController = true;
	}
}