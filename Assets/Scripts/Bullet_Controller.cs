﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Controller : MonoBehaviour
{
    private Rigidbody projectile;
    public GameObject decal;
    
    public float speedMetresPerSecond;
    public int damagePerHit;

	private Vector3 fwd; //added this as a class instance variable as it was repeatedly being created in the Fixed Update

    void Awake()
    {
        projectile = GetComponent<Rigidbody>();
    }

	private void Start()
	{
		Destroy(gameObject, 3f); //moved from the fixed update as only needs to be called once
		fwd = transform.TransformDirection(Vector3.forward); //only needs to be called once
	}

	void FixedUpdate()
    {
        projectile.velocity = fwd * speedMetresPerSecond;        
    }
     void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponentInParent<Drone_Stats>().updateHealthValue(damagePerHit);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Crit")
        {
            other.gameObject.GetComponentInParent<Drone_Stats>().updateHealthValue(damagePerHit * 2);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "Environment")
        {
            Instantiate(decal, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}